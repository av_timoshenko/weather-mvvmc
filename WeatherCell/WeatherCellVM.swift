//
//  WeatherCellVM.swift
//  weather-mvvmc
//
//  Created by toor on 9/1/19.
//  Copyright © 2019 toor. All rights reserved.
//
import Action
import RxSwift
import XCoordinator

protocol WeatherCellVMInput {}

protocol WeatherCellVMOutput {
    var dayOfWeek: Variable<String> {get}
    var date: Variable<String> {get}
    var sunrise: Variable<String> {get}
    var sunset: Variable<String> {get}
    
    var timeOfDayMorning: Variable<String> {get}
    var imageMorning: Variable<UIImage> {get}
    var tempMorning: Variable<String> {get}
    
    var timeOfDayDay: Variable<String> {get}
    var imageDay: Variable<UIImage> {get}
    var tempDay: Variable<String> {get}
    
    var timeOfDayEvening: Variable<String> {get}
    var imageEvening: Variable<UIImage> {get}
    var tempEvening: Variable<String> {get}
    
    var timeOfDayNight: Variable<String> {get}
    var imageNight: Variable<UIImage> {get}
    var tempNight: Variable<String> {get}
}

protocol WeatherCellVM {
    var input: WeatherCellVMInput { get }
    var output: WeatherCellVMOutput { get }
}

extension WeatherCellVM where Self: WeatherCellVMInput & WeatherCellVMOutput {
    var input: WeatherCellVMInput { return self }
    var output: WeatherCellVMOutput { return self }
}
