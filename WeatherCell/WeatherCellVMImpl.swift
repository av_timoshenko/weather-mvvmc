//
//  WeatherCellVMImpl.swift
//  weather-mvvmc
//
//  Created by toor on 9/13/19.
//  Copyright © 2019 toor. All rights reserved.
//

import Foundation
import RxSwift

class WeatherCellVMImpl: WeatherCellVM, WeatherCellVMInput, WeatherCellVMOutput {
    var dayOfWeek = Variable<String>("")
    
    var date = Variable<String>("")
    
    var sunrise = Variable<String>("")
    
    var sunset = Variable<String>("")
    
    
    var timeOfDayMorning = Variable<String>("")
    
    var imageMorning = Variable<UIImage>(UIImage())
    
    var tempMorning = Variable<String>("")
    
    
    var timeOfDayDay = Variable<String>("")
    
    var imageDay = Variable<UIImage>(UIImage())
    
    var tempDay = Variable<String>("")
    
    
    var timeOfDayEvening = Variable<String>("")
    
    var imageEvening = Variable<UIImage>(UIImage())
    
    var tempEvening = Variable<String>("")
    
    
    var timeOfDayNight = Variable<String>("")
    
    var imageNight = Variable<UIImage>(UIImage())
    
    var tempNight = Variable<String>("")
    
    var viewModel: HistoryVM!
    
    init(wd: WeatherDay) {
        self.dayOfWeek.value = wd.dayOfWeek
        self.date.value = wd.date
        self.sunrise.value = wd.sunrise
        self.sunset.value = wd.sunset
        
        self.timeOfDayMorning.value = wd.morning.timeOfDay
        self.imageMorning.value = UIImage(data: (wd.morning.imgData!)) ?? UIImage()
        self.tempMorning.value = wd.morning.temp
        
        self.timeOfDayDay.value = wd.day.timeOfDay
        self.imageDay.value = UIImage(data: (wd.day.imgData!)) ?? UIImage()
        self.tempDay.value = wd.day.temp
        
        self.timeOfDayEvening.value = wd.evenig.timeOfDay
        self.imageEvening.value = UIImage(data: (wd.evenig.imgData!)) ?? UIImage()
        self.tempEvening.value = wd.evenig.temp
        
        self.timeOfDayNight.value = wd.night.timeOfDay
        self.imageNight.value = UIImage(data: (wd.night.imgData!)) ?? UIImage()
        self.tempNight.value = wd.night.temp
    }
    
}
