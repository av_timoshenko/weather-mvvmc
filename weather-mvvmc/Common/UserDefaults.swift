//
//  UserDefaults .swift
//  weather-mvc
//
//  Created by toor on 16.08.2019.
//  Copyright © 2019 toor. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

extension DefaultsKeys {
    static let latitude     = DefaultsKey<Double?>("latitude")
    static let longitude    = DefaultsKey<Double?>("longitude")
    static let addr         = DefaultsKey<String?>("addr")
    static let cities       = DefaultsKey<[String:String]?>("cities")
    static let cityCode     = DefaultsKey<String?>("cityCode")
}
