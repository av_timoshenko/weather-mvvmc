//
//  HistoryCoordinator.swift
//  weather-mvvmc
//
//  Created by toor on 9/2/19.
//  Copyright © 2019 toor. All rights reserved.
//

import Foundation
import XCoordinator

enum HistoryRoute : Route {
    case history
}

class HistoryCoordinator: NavigationCoordinator<HistoryRoute> {
    private let wdManager: WeatherDayManager
    
    init(wdManager: WeatherDayManager) {
        self.wdManager = wdManager
        super.init(initialRoute: .history)
    }
    
    override func prepareTransition(for route: HistoryRoute) -> NavigationTransition {
        switch route {
        case .history:
            let viewController = HistoryVC.instantiateFromNib()
            let viewModel = HistoryVMImpl(router: anyRouter, wdManager: wdManager)
            viewController.bind(to: viewModel)
            return .push(viewController)
        }
    }
    
}
