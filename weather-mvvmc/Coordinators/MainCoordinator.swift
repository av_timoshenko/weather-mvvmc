//
//  MainCoordinator.swift
//  weather-mvvmc
//
//  Created by toor on 9/1/19.
//  Copyright © 2019 toor. All rights reserved.
//

import Foundation
import XCoordinator

enum MainRoute : Route {
    case settings
    case history
}

class MainCoordinator: TabBarCoordinator<MainRoute> {
    
    private let settingsRouter : AnyRouter<SettingsRoute>
    private let historyRouter : AnyRouter<HistoryRoute>
 
    convenience init() {
        let wdManager = WeatherDayManager()
        
        let settingsCoordinator = SettingsCoordinator(wdManager: wdManager)
        settingsCoordinator.rootViewController.tabBarItem = UITabBarItem(title: "Настройки", image: nil, tag: 1)
        settingsCoordinator.rootViewController.isNavigationBarHidden = true
        
        let historyCoordinator = HistoryCoordinator(wdManager: wdManager)
        historyCoordinator.rootViewController.tabBarItem = UITabBarItem(title: "История", image: nil, tag: 2)
        historyCoordinator.rootViewController.isNavigationBarHidden = true
        
        self.init(settingsRouter: settingsCoordinator.anyRouter, historyRouter: historyCoordinator.anyRouter)
    }
    
    init(settingsRouter: AnyRouter<SettingsRoute>,
         historyRouter: AnyRouter<HistoryRoute>) {
        self.settingsRouter = settingsRouter
        self.historyRouter  = historyRouter
        
        super.init(tabs: [settingsRouter, historyRouter], select: settingsRouter)
    }
    
    override func prepareTransition(for route: MainRoute) -> TabBarTransition {
        switch route {
        case .settings:
            return .select(settingsRouter)
        case .history:
            return .select(historyRouter)
            
        }
    }
}
