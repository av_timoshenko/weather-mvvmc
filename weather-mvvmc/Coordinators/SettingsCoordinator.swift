//
//  SettingsCoordinator.swift
//  weather-mvvmc
//
//  Created by toor on 9/2/19.
//  Copyright © 2019 toor. All rights reserved.
//

import Foundation
import XCoordinator

enum SettingsRoute : Route {
    case settings
}

class SettingsCoordinator: NavigationCoordinator<SettingsRoute> {
    private let wdManager : WeatherDayManager
    
    init(wdManager : WeatherDayManager) {
        self.wdManager = wdManager
        super.init(initialRoute: .settings)
    }
    
    override func prepareTransition(for route: SettingsRoute) -> NavigationTransition {
        switch route {
        case .settings:
            let viewController = SettingsVC.instantiateFromNib()
            let viewModel = SettingsVMImpl(router: anyRouter, wdManager: wdManager)
            viewController.bind(to: viewModel)
            return .push(viewController)
        }
    }

}
