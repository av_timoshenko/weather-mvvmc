//
//  WeatherDayManager.swift
//  weather-mvc
//
//  Created by toor on 8/26/19.
//  Copyright © 2019 toor. All rights reserved.
//

import Foundation
import RxSwift

class WeatherDays {
    private var weatherDays: [Int:WeatherDay] = [:]
    private let lock = NSLock()
    
    public var count: Int {
        return weatherDays.count
    }
    
    subscript(index: Int) -> WeatherDay{
        get{
            lock.lock()
            let retVal =  weatherDays[index]!
            lock.unlock()
            return retVal
        }
        set (newValue) {
            lock.lock()
            weatherDays[index] = newValue
            lock.unlock()
        }
    }
    
    func removeAll(){
        lock.lock()
        weatherDays.removeAll()
        lock.unlock()
    }
}

class WeatherDayManager {
    let parallelQueue = DispatchQueue.init(label: "parallel", qos: .userInitiated, attributes: [.concurrent])
    let serialQueue = DispatchQueue.init(label: "serial", qos: .userInitiated)
    
    var count = 0
    var weatherDays = WeatherDays()
    var curentDay: Date = Calendar.current.startOfDay(for: Date())
    var SiteCityCode : String? = "26850" {
        willSet (newValue) {
            if newValue != self.SiteCityCode { //clear all data
                clearData()
            }
        }
    }
    
    var downloadObservable : Variable<Int> = Variable(0)
    
    
    static var mainLock  = NSLock()
    
    subscript(index: Int) -> WeatherDay {
        get {
            return weatherDays[index]
            
        }
    }
    
    func clearData()  {
        weatherDays.removeAll()
        count = 0
        curentDay =  Calendar.current.startOfDay(for: Date())
        downloadObservable.value = 0
    }
    
    fileprivate func downloadJob(_ curDay: TimeInterval,_ taskCounter:Int) {
        let data = WGet.download(urlString: "https://pogoda.tut.by/archive.html?date_from=\(curDay)&date_to=\(curDay)&city=\(self.SiteCityCode!)")
        
        let wd = WeatherParser.parse(data)
        if let weatherData = wd {
            self.weatherDays[taskCounter] = weatherData
        }
    }
    
    fileprivate func executeBatch(_ batchIndex: Int, _ batchSize: Int) {
        let initCount = self.count
        let dg = DispatchGroup()
        for taskIndex in 0...9 {
            dg.enter()
            let taskCounter = initCount + batchIndex*10 + taskIndex
            let curDay = Calendar.current.date(byAdding: .day, value: -(taskCounter),to: self.curentDay)!.timeIntervalSince1970
            self.parallelQueue.async(group: dg){
                self.downloadJob(curDay, taskCounter)
                dg.leave()
            }
            
        }
        
        //wait variant
        dg.wait()
        if(batchIndex == batchSize) {
            self.count  =  self.weatherDays.count
            self.downloadObservable.value = 0
            WeatherDayManager.mainLock.unlock()
        }
 
        //notify variant
        /*
        dg.notify(queue: .global()) {
            if(batchIndex == batchSize) {
                self.count  =  self.weatherDays.count
                self.downloadObservable.value = 0
                WeatherDayManager.mainLock.unlock()
            }
        }*/
    }
    
    func next(batchSize: Int) {
        WeatherDayManager.mainLock.lock()
        for batchIndex in 0...batchSize {
            self.serialQueue.async {
                self.executeBatch(batchIndex, batchSize)
            }
        }
        
    }
    
    func nextOld(batchSize: Int) {
        WeatherDayManager.mainLock.lock()
        let semaphore = DispatchSemaphore(value: 1)
        var batchCount = 0
        let countDaysAtStart = self.weatherDays.count
        
        for batchIndex in 1...batchSize {
            DispatchQueue.global().async {
                //first cuncurent section
                semaphore.wait()
                let insertIndex = countDaysAtStart + batchIndex - 1
                self.curentDay = Calendar.current.date(byAdding: .day, value: -1,to: self.curentDay)!
                semaphore.signal()
                
                let data = WGet.download(urlString: "https://pogoda.tut.by/archive.html?date_from=\(self.curentDay.timeIntervalSince1970)&date_to=\(self.curentDay.timeIntervalSince1970)&city=\(self.SiteCityCode!)")
                
                let wd = WeatherParser.parse(data)
               
                //second cuncurent section
                semaphore.wait()
                if let weatherData = wd {
                    self.weatherDays[insertIndex] = weatherData
                    self.count  =  self.weatherDays.count
                }
                batchCount += 1
                
                if batchCount == batchSize {
                    self.downloadObservable.value = batchSize
                    WeatherDayManager.mainLock.unlock()
                }
                semaphore.signal()
            }
        }
    }
    
    
}

