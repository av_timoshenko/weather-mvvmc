//
//  HistoryVC.swift
//  weather-mvvmc
//
//  Created by toor on 9/1/19.
//  Copyright © 2019 toor. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class HistoryVC: UIViewController, BindableType {
    
    var viewModel: HistoryVM!
    
    private let disposeBag = DisposeBag()
    private let tableViewCellIdentifier = String(describing: WeatherCell.self)
    
    @IBOutlet weak var tableView: UITableView!
    
    func bindViewModel() {
        viewModel.output.needReloadTable.asObservable().subscribe { (param) in
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }.disposed(by: disposeBag)
    
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "WeatherCell", bundle: nil), forCellReuseIdentifier: "WeatherCell")
        tableView.register(UINib(nibName: "EmptyCell", bundle: nil), forCellReuseIdentifier: "EmptyCell")
        tableView.rowHeight = 120
        tableView.dataSource = self
    }
    
}

extension HistoryVC: UITableViewDataSource {
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.output.countDataObjects + 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let days = viewModel.output.days
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherCell", for: indexPath) as! WeatherCell
        
        if (indexPath.row == viewModel.output.countDataObjects) {
            viewModel.input.needFetchBanch.onNext(())
            return tableView.dequeueReusableCell(withIdentifier: "EmptyCell", for: indexPath) as! EmptyCell
        } else {
            cell.bind(to: days[indexPath.row])
        }
        
        return cell
    }
}
