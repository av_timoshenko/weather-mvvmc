//
//  HistoryVM.swift
//  weather-mvvmc
//
//  Created by toor on 9/1/19.
//  Copyright © 2019 toor. All rights reserved.
//
import Action
import RxSwift
import XCoordinator

protocol HistoryVMInput {
    var needFetchBanch: InputSubject<Void> {get}
}

protocol HistoryVMOutput {
    var days: [WeatherCellVMImpl] {get}
    var countDataObjects: Int {get}
    var needReloadTable: Variable<Bool> {get}
}

protocol HistoryVM {
    var input: HistoryVMInput { get }
    var output: HistoryVMOutput { get }
}

extension HistoryVM where Self: HistoryVMInput & HistoryVMOutput {
    var input: HistoryVMInput { return self }
    var output: HistoryVMOutput { return self }
}
