//
//  HistoryVMImpl.swift
//  weather-mvvmc
//
//  Created by toor on 9/1/19.
//  Copyright © 2019 toor. All rights reserved.
//

import Foundation
import Action
import RxSwift
import RxDataSources
import XCoordinator

class HistoryVMImpl: HistoryVM, HistoryVMInput, HistoryVMOutput {
    private let wdManager: WeatherDayManager
    private let disposeBag = DisposeBag()
    
    var days: [WeatherCellVMImpl] = []
    
    var needReloadTable: Variable<Bool> = Variable(true)
    
    var countDataObjects: Int = 0
    
    private(set) lazy var needFetchBanch: InputSubject<Void> = needFetchBanchAction.inputs
    
    private lazy var needFetchBanchAction = CocoaAction { [unowned self] in
        if self.wdManager.count <= 5 {
            self.wdManager.next(batchSize: 0)
        } else {
            self.wdManager.next(batchSize: 2)
        }
        
        return Observable.empty()
    }
    
    func applayModel()  {
        self.countDataObjects = wdManager.count
        var dEndIndex = self.days.endIndex
        while self.days.count != wdManager.count {
            self.days.append(WeatherCellVMImpl(wd: wdManager.weatherDays[dEndIndex]))
            dEndIndex += 1
        }
    }
    
    // MARK: - Private
    private let router: AnyRouter<HistoryRoute>
    
    // MARK: - Init
    init(router: AnyRouter<HistoryRoute>, wdManager: WeatherDayManager) {
        self.wdManager = wdManager
        self.router = router
        
        wdManager.downloadObservable.asObservable()
            .observeOn(MainScheduler.instance)
            .subscribeOn(MainScheduler.instance)
            .subscribe { (bathSize) in
            self.applayModel()
            self.needReloadTable.value = true
        }.disposed(by: disposeBag)
    }
}



