//
//  SettingsVC.swift
//  weather-mvvmc
//
//  Created by toor on 9/1/19.
//  Copyright © 2019 toor. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import YandexMapKit

class SettingsVC: UIViewController {
    
    var viewModel: SettingsVM!
    private let disposeBag = DisposeBag()
    
    @IBOutlet weak var lblLatitude: UILabel!
    @IBOutlet weak var lblLongitude: UILabel!
    @IBOutlet weak var lblAdress: UILabel!
    
    @IBOutlet weak var mapView: YMKMapView!
    
    @IBOutlet weak var btnGetCurentPlace: UIButton!
    @IBOutlet weak var btnSaveSettings: UIButton!
    
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        YMKMapKit.setApiKey("6431e60a-7bbc-4d95-aab9-1047a3695aa4")
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.mapWindow.map.addTapListener(with: self)
    }
    
    func moveMapToCurrentPosition(curentPosition: YMKPoint){
        DispatchQueue.main.async {
            self.mapView.mapWindow.map.move(
                with: YMKCameraPosition(target: curentPosition, zoom: 18, azimuth: 0, tilt: 0),
                animationType: YMKAnimation(type: YMKAnimationType.linear, duration: 2),
                cameraCallback: nil)
            
            self.mapView.mapWindow.map.mapObjects.clear()
            self.mapView.mapWindow.map.mapObjects.addPlacemark(with: curentPosition)
            
        }
    }

}

extension SettingsVC: BindableType {
    func bindViewModel() {
        btnSaveSettings.rx.tap.subscribe({ (Event) in
            self.viewModel.input.saveSettingsTrigger.onNext(())
            let tabBarController = self.parent?.parent as! UITabBarController
            tabBarController.selectedIndex = 1
        }).disposed(by: disposeBag)
        
        btnGetCurentPlace.rx.tap
            .bind(to: viewModel.input.getCurrentPlaceTrigger)
            .disposed(by: disposeBag)
        
        viewModel.output.addr.asObservable()
            .bind(to: lblAdress.rx.text)
            .disposed(by: disposeBag)
       
        viewModel.output.latitude.asObservable()
            .bind(to: lblLatitude.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.output.longitude.asObservable()
            .bind(to: lblLongitude.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.output.curentPosition.asObservable()
            .subscribe { (point) in
            self.moveMapToCurrentPosition(curentPosition: point.element ?? YMKPoint())
        }.disposed(by: disposeBag)
       
    }
}

extension SettingsVC : YMKLayersGeoObjectTapListener {
    func onObjectTap(with event: YMKGeoObjectTapEvent) -> Bool {
        viewModel.input.mapTapTrigger.onNext(event)
        return true
    }
}


