//
//  LoginViewModel.swift
//  XCoordinator_Example
//
//  Created by Joan Disho on 03.05.18.
//  Copyright © 2018 QuickBird Studios. All rights reserved.
//
import Action
import RxSwift
import XCoordinator
import YandexMapKit

protocol SettingsVMInput {
    var getCurrentPlaceTrigger : InputSubject<Void> {get}
    var saveSettingsTrigger : InputSubject<Void> {get}
    var mapTapTrigger : InputSubject<YMKGeoObjectTapEvent> {get}
}

protocol SettingsVMOutput {
    var addr: Variable<String>{get}
    var latitude: Variable<String>{get}
    var longitude: Variable<String>{get}
    var curentPosition: Variable<YMKPoint>{get}
}

protocol SettingsVM {
    var input: SettingsVMInput { get }
    var output: SettingsVMOutput { get }
}

extension SettingsVM where Self: SettingsVMInput & SettingsVMOutput {
    var input: SettingsVMInput { return self }
    var output: SettingsVMOutput { return self }
}
