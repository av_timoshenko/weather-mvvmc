//
//  SettingsVMImpl.swift
//  weather-mvvmc
//
//  Created by toor on 9/1/19.
//  Copyright © 2019 toor. All rights reserved.
//

import Foundation
import Action
import RxSwift
import XCoordinator
import YandexMapKit
import SwiftyJSON
import SwiftyUserDefaults

class SettingsVMImpl: SettingsVM, SettingsVMInput, SettingsVMOutput {
    
    private let wdManager : WeatherDayManager
    
    private let disposeBag = DisposeBag()
    
    private var locationManager : LocationManager
    
    private(set) lazy var getCurrentPlaceTrigger: InputSubject<Void> = getCurrentPlaceAction.inputs
    
    private(set) lazy var saveSettingsTrigger: InputSubject<Void> = saveSettingsAction.inputs
    
    private(set) lazy var mapTapTrigger: InputSubject<YMKGeoObjectTapEvent> = mapTapAction.inputs
    
    private(set) lazy var curentPosition: Variable<YMKPoint> = Variable(YMKPoint())
    
    private(set) lazy var addr: Variable<String> = Variable("")
    
    private(set) lazy var latitude: Variable<String> = Variable("")
    
    private(set) lazy var longitude: Variable<String> = Variable("")
    
   
    private lazy var getCurrentPlaceAction = CocoaAction { [unowned self] in
        self.locationManager.update()
        return Observable.empty()
    }
    
    private lazy var saveSettingsAction = CocoaAction { [unowned self] in
        self.saveToDefaults()
        return Observable.empty()
    }
    
    private lazy var mapTapAction = Action<YMKGeoObjectTapEvent, Void>{ [unowned self] valYMKGeoObjectTapEvent in
        self.curentPosition.value = valYMKGeoObjectTapEvent.geoObject.geometry[0].point!
        return Observable.empty()
    }
    
   
    // MARK: - Private
    private let router: AnyRouter<SettingsRoute>
    
    // MARK: - Init
    init(router: AnyRouter<SettingsRoute>, wdManager :  WeatherDayManager) {
        self.router = router
        self.wdManager = wdManager
        
        self.locationManager = LocationManager()
        self.locationManager.delegate = self
        
        self.restoreFromDefaults()
        self.curentPosition.asObservable().bind(onNext: { (point) in
            self.addr.value = "Адрес: \(Geocoding.getAddres(latitude: point.latitude, longitude: point.longitude))"
            self.latitude.value  = "Широта: \(String(format: "%.4f",point.latitude))"
            self.longitude.value = "Долгота: \(String(format: "%.4f",point.longitude))"
        }).disposed(by: disposeBag)
        
    }
    
    func restoreFromDefaults() {
        self.curentPosition.value = YMKPoint(latitude: Defaults[.latitude] ?? 53.894055, longitude: Defaults[.longitude] ?? 27.419439)
        self.addr.value = Defaults[.addr] ?? "Беларусь, Минск, улица Скрипникова, 29"
    }
    
    func saveToDefaults() {        
        Defaults[.latitude]         = self.curentPosition.value.latitude
        Defaults[.longitude]        = self.curentPosition.value.longitude
        Defaults[.addr]             = self.addr.value
        Defaults[.cityCode]         = Cities.getCityCode(addr.value)
        wdManager.SiteCityCode      = Defaults[.cityCode]
    }
}


extension SettingsVMImpl : LocationUpdatedProtocol {
    func onLocationUpdated(with location: YMKLocation) {
        self.curentPosition.value = location.position
    }
}


