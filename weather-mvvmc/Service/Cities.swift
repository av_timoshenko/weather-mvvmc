//
//  Cities.swift
//  weather-mvc
//
//  Created by toor on 8/28/19.
//  Copyright © 2019 toor. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

class Cities {
    static func getCityCode(_ adrr: String) -> String? {
        var ccode = Defaults[.cities]
        let curentDay = Date()
        if ccode == nil {
            let data = WGet.download(urlString: "https://pogoda.tut.by/archive.html?date_from=\(curentDay.timeIntervalSince1970)&date_to=\(curentDay.timeIntervalSince1970)&city=26850")
            ccode = WeatherParser.parseCities(data)
            Defaults[.cities] = ccode
        }
        
        if ccode != nil {
            
            for code in ccode! {
                if adrr.lowercased().contains(code.key.lowercased()) {
                    return code.value
                }
            }
        }
        
        return nil
    }
    
}
