//
//  Geocoding.swift
//  weather-mvvmc
//
//  Created by toor on 9/4/19.
//  Copyright © 2019 toor. All rights reserved.
//

import Foundation
import SwiftyJSON

class Geocoding {
    static func getAddres(latitude: Double, longitude: Double) -> String {
        var retStr = ""
        
        let urlString = "https://geocode-maps.yandex.ru/1.x/?apikey=9e02d696-b1ba-4198-9afc-68db4768fc18&format=json&geocode=\(longitude),\(latitude)"
        
        let responseData = WGet.download(urlString: urlString)
        
        if let rd = responseData {
            do {
                let json = try JSON(data: rd)
                retStr = json["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["metaDataProperty"]["GeocoderMetaData"]["text"].string ?? ""
                
            } catch {
                retStr = ""
            }
        }
        
        return retStr
    }
}
