//
//  LocationManager.swift
//  weather-mvvmc
//
//  Created by toor on 9/4/19.
//  Copyright © 2019 toor. All rights reserved.
//

import Foundation
import YandexMapKit

class LocationManager: NSObject {
    var locationManager: YMKLocationManager = YMKMapKit.sharedInstance()!.createLocationManager()
    var delegate : LocationUpdatedProtocol? = nil
    
    override init() {
        super.init()
    }
    
    func update(){
        locationManager.requestSingleUpdate(withLocationListener: self)
    }
}


extension LocationManager : YMKLocationDelegate {
    func onLocationStatusUpdated(with status: YMKLocationStatus) {
        
    }
    
    func onLocationUpdated(with location: YMKLocation) {
        if delegate != nil {
            delegate?.onLocationUpdated(with: location)
        }
    }
}


protocol LocationUpdatedProtocol {
    func onLocationUpdated(with location: YMKLocation)
}
