//
//  WGet.swift
//  weather-mvvmc
//
//  Created by toor on 9/2/19.
//  Copyright © 2019 toor. All rights reserved.
//

import Foundation
import Alamofire


class WGet {
     static func download(urlString: String) -> Data? {
        var retData: Data?
        
        let semaphore = DispatchSemaphore(value: 0)
        request(urlString).responseData(queue: DispatchQueue.global(qos: .userInteractive) ,completionHandler: {(response) in
            retData = response.data
            semaphore.signal()
        })
        semaphore.wait()
        
        return retData
    }
}
