//
//  WeatherParser.swift
//  weather-mvc
//
//  Created by toor on 8/26/19.
//  Copyright © 2019 toor. All rights reserved.
//

import Foundation
import Ji

class WeatherParser {
    static func parse(_ data: Data?) -> WeatherDay? {
        guard let pd = data else {
            return nil
        }
        
        guard let htmlSrtring = String(data: pd, encoding: String.Encoding.utf8) else {
            return nil
        }
        
        
        let jiDoc = Ji(htmlString: htmlSrtring)
        let pDate = jiDoc?.xPath("//table[@class=\"b-future__t\"]/tbody/tr/th/span[@class=\"future-day\"]/text()")?[0].content
        let pDayOfWeek = jiDoc?.xPath("//table[@class=\"b-future__t\"]/tbody/tr/th/text()")?[0].content
        let pSun = jiDoc?.xPath("//table[@class=\"b-future__t\"]/tfoot/tr/th/text()")
        let pTimeOfDay = jiDoc?.xPath("//table[@class=\"b-future__t\"]/tbody/tr/td/div/div/div/div/text()")
        let pUrl = jiDoc?.xPath("//table[@class=\"b-future__t\"]/tbody/tr/td/div/div/div/img")
        let pTemp = jiDoc?.xPath("//table[@class=\"b-future__t\"]/tbody/tr/td/div/div/div/span/text()")
        let pDetail = jiDoc?.xPath("//table[@class=\"b-future__t\"]/tbody/tr/td/div[@class=\"future-detail m-mediamin\"]/div/text()")
        
        let morning     = WeatherItem(timeOfDay: pTimeOfDay![0].content!.clearSpecSymbol()
            , urlWeatherIcon: pUrl![0].attributes["src"]!.clearSpecSymbol()
            , temp: pTemp![0].content!.clearSpecSymbol()
            , humidity: pDetail![0].content!.clearSpecSymbol()
            , pressure: pDetail![2].content!.clearSpecSymbol()
            , wind: pDetail![3].content!.clearSpecSymbol()
            , imgData: WGet.download(urlString: pUrl![0].attributes["src"]!.clearSpecSymbol()))
        
        let day         = WeatherItem(timeOfDay: pTimeOfDay![1].content!.clearSpecSymbol()
            , urlWeatherIcon: pUrl![1].attributes["src"]!.clearSpecSymbol()
            , temp: pTemp![0].content!.clearSpecSymbol()
            , humidity: pDetail![4].content!.clearSpecSymbol()
            , pressure: pDetail![6].content!.clearSpecSymbol()
            , wind: pDetail![7].content!.clearSpecSymbol()
            , imgData: WGet.download(urlString: pUrl![1].attributes["src"]!.clearSpecSymbol()))
        
        let evenig      = WeatherItem(timeOfDay: pTimeOfDay![2].content!.clearSpecSymbol()
            , urlWeatherIcon: pUrl![2].attributes["src"]!.clearSpecSymbol()
            , temp: pTemp![0].content!.clearSpecSymbol()
            , humidity: pDetail![8].content!.clearSpecSymbol()
            , pressure: pDetail![10].content!.clearSpecSymbol()
            , wind: pDetail![11].content!.clearSpecSymbol()
            , imgData: WGet.download(urlString: pUrl![2].attributes["src"]!.clearSpecSymbol()))
        
        let night       = WeatherItem(timeOfDay: pTimeOfDay![3].content!.clearSpecSymbol()
            , urlWeatherIcon: pUrl![3].attributes["src"]!.clearSpecSymbol()
            , temp: pTemp![0].content!.clearSpecSymbol()
            , humidity: pDetail![12].content!.clearSpecSymbol()
            , pressure: pDetail![14].content!.clearSpecSymbol()
            , wind: pDetail![15].content!.clearSpecSymbol()
            , imgData: WGet.download(urlString: pUrl![3].attributes["src"]!.clearSpecSymbol()))
        
        return WeatherDay(date: pDate!.clearSpecSymbol(), dayOfWeek: pDayOfWeek!.clearSpecSymbol() ,sunrise: pSun![0].content!.clearSpecSymbol(), sunset: pSun![1].content!.clearSpecSymbol(), morning: morning, day: day, evenig: evenig, night: night)
        
    }
    
    static func parseCities(_ data: Data?) -> [String:String]? {
        guard let pd = data else {
            return nil
        }
        
        guard let htmlSrtring = String(data: pd, encoding: String.Encoding.utf8) else {
            return nil
        }
        
        let jiDoc = Ji(htmlString: htmlSrtring)
        let pCities = jiDoc?.xPath("//select[@name=\"city\"]/optgroup/option")
        
        var ret:[String:String] = [:]
        for pCity in pCities ?? [] {
            ret[pCity.content!.clearSpecSymbol().lowercased()] = pCity.attributes["value"]
        }
        
        return ret
    }
}


extension String {
    func clearSpecSymbol() -> String {
        return self.replacingOccurrences(of: "\n", with: "", options: .literal, range: nil)
            .replacingOccurrences(of: "\r", with: "", options: .literal, range: nil)
            .trimmingCharacters(in: .whitespacesAndNewlines)
        
    }
}
