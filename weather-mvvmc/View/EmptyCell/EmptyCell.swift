//
//  EmptyCell.swift
//  weather-mvvmc
//
//  Created by toor on 9/4/19.
//  Copyright © 2019 toor. All rights reserved.
//

import UIKit

class EmptyCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
