//
//  	WeatherCell.swift
//  weather-mvc
//
//  Created by toor on 8/28/19.
//  Copyright © 2019 toor. All rights reserved.
//

import UIKit
import RxSwift

class WeatherCell: UITableViewCell, BindableType {
    
    var viewModel: WeatherCellVM!
   
    private let disposeBag = DisposeBag()

    @IBOutlet weak var dayOfWeekLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var sunriseLabel: UILabel!
    
    @IBOutlet weak var sunsetLabel: UILabel!
    
  
    //Morning
    @IBOutlet weak var timeOfDayLabelMorning: UILabel!
    
    @IBOutlet weak var imageMorning: UIImageView!
    
    @IBOutlet weak var tempLabelMorning: UILabel!
    
    //Day
    @IBOutlet weak var timeOfDayLabelDay: UILabel!
    
    @IBOutlet weak var imageDay: UIImageView!
    
    @IBOutlet weak var tempLabelDay: UILabel!
    
    //Evening
    @IBOutlet weak var timeOfDayLabelEvening: UILabel!
    
    @IBOutlet weak var imageEvening: UIImageView!
    
    @IBOutlet weak var tempLabelEvening: UILabel!
    
    //Night
    @IBOutlet weak var timeOfDayLabelNight: UILabel!
    
    @IBOutlet weak var imageNight: UIImageView!
    
    @IBOutlet weak var tempLabelNight: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bindViewModel() {
        viewModel.output.dayOfWeek
            .asObservable()
            .bind(to: self.dayOfWeekLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.output.date
            .asObservable()
            .bind(to: self.dateLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.output.sunrise
            .asObservable()
            .bind(to: self.sunriseLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.output.sunset
            .asObservable()
            .bind(to: self.sunsetLabel.rx.text)
            .disposed(by: disposeBag)
        
        
        viewModel.output.timeOfDayMorning
            .asObservable()
            .bind(to: self.timeOfDayLabelMorning.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.output.imageMorning
            .asObservable()
            .bind(to: self.imageMorning.rx.image)
            .disposed(by: disposeBag)
        
        viewModel.output.tempMorning
            .asObservable()
            .bind(to: self.tempLabelMorning.rx.text)
            .disposed(by: disposeBag)
        
        
        
        viewModel.output.timeOfDayDay
            .asObservable()
            .bind(to: self.timeOfDayLabelDay.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.output.imageDay
            .asObservable()
            .bind(to: self.imageDay.rx.image)
            .disposed(by: disposeBag)
        
        viewModel.output.tempDay
            .asObservable()
            .bind(to: self.tempLabelDay.rx.text)
            .disposed(by: disposeBag)
        
        
        viewModel.output.timeOfDayEvening
            .asObservable()
            .bind(to: self.timeOfDayLabelEvening.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.output.imageEvening
            .asObservable()
            .bind(to: self.imageEvening.rx.image)
            .disposed(by: disposeBag)
        
        viewModel.output.tempEvening
            .asObservable()
            .bind(to: self.tempLabelEvening.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.output.timeOfDayNight
            .asObservable()
            .bind(to: self.timeOfDayLabelNight.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.output.imageNight
            .asObservable()
            .bind(to: self.imageNight.rx.image)
            .disposed(by: disposeBag)
        
        viewModel.output.tempNight
            .asObservable()
            .bind(to: self.tempLabelNight.rx.text)
            .disposed(by: disposeBag)
    }
    
}
